from pymongo import MongoClient
import pandas as pd

from config import (MONGO_USER, MONGO_PASSWORD, MONGO_HOST, MONGO_PORT,
                    MONGO_DB, MONGO_COLLECTION, CSV_FILE_TO_READ,
                    CSV_FILE_TO_WRITE, log)


def connect_mongo_db():
    """Connection to Mongo DB."""
    try_num = 0
    while try_num < 2:
        try:
            mongo_uri = 'mongodb://{0}:{1}@{2}:{3}/{4}'.format(
                MONGO_USER, MONGO_PASSWORD, MONGO_HOST, MONGO_PORT, MONGO_DB)
            conn = MongoClient(mongo_uri)
            return conn[MONGO_DB]
        except ConnectionError as e:
            try_num += 1
            log.error('Try: {0} | Error: {1}'.format(try_num, e))
        except Exception as e:
            log.error(e)
    print('Сan not connect to the database')


def read_mongo_db(conn, address):
    """
    Read from Mongo DB and store in dataframe.
    :param conn: connection to Mongo DB
    :param address: address of wallet in DB
    """
    df = pd.DataFrame(list(conn[MONGO_COLLECTION].find(
        {'address': address})))
    df = df.drop(columns=['_id'])
    return df


if __name__ == '__main__':
    conn = connect_mongo_db()
    i = 0
    for row in pd.read_csv(CSV_FILE_TO_READ).address:
        try:
            df = read_mongo_db(conn, row)
            df = df.groupby(['date', 'address'])['balance'].last()
            df.fillna(0).to_csv(CSV_FILE_TO_WRITE,
                                mode='a',
                                float_format='%f',
                                header=False)
            i += 1
            print(i)
        except KeyError as e:
            log.error('Error with obj: {0} | {1}'.format(row, e))
        except Exception as e:
            log.error(e)
        finally:
            pass
